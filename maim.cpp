#include <iostream>

class stack
{
	int* array;
	int last_index = 0;
	int size;

public:
	stack(int new_size)
	{
		size = new_size;
		array = new int[size];
	}

	~stack() 
	{
		delete[] array;
	}

	void push(int new_elem)
	{
		if (last_index < size)
		{
			array[last_index] = new_elem;
			last_index++;
		}
	}

	int top()
	{
		if (last_index < 0)
		{
			throw std::out_of_range("stask is empty");
		}
		else
		{
			return array[last_index -1];
		}
	}

	void pop()
	{
		if (last_index > 0)
		{
			--last_index;
		}
	}
	
};

int main()
{
	stack stk(20);
	for (int i = 0; i < 20; i++)
	{
		stk.push(i);
		std::cout << stk.top() << std::endl;
	}
	for (int i = 0; i < 50; ++i);
	{
		stk.pop();
		std::cout << stk.top() << std::endl;
	}
	
}
